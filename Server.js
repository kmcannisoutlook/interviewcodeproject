const express = require('express');
const app = express();
const fs = require('fs');
const jsonStandardTemplate = 'templates.json';
const jsonExtendedTemplate = 'extendedTemplate.json';
const jsonTemplate = jsonStandardTemplate;

const jsonData = fs.readFileSync(`./data/${jsonTemplate}`, 'utf8', (err, data) => {
    if (err) {
        throw err;
    }
    return JSON.parse(data);
});

//Route returns total count of templates
app.get('/api/template/count', (req, res) => {
    var count = JSON.parse(jsonData).length;
    res.send({count});
});

//Route parameter id, returns a single json object that has the same id value
app.get('/api/template/:id', (req, res) => {
        var val = req.params.id;        
        var index = 0;

        if (val !== '0') {
            index = JSON.parse(jsonData).findIndex(function(item, i){
                return item.id === val
            });
        }

        var array = JSON.parse(jsonData)[index];

        res.send(array);
});

//Route parameter page, returns up to four template objects
app.get('/api/filmstrip/:page', (req, res) => {
        var val = Number(req.params.page);
        var array = [];
        var count = JSON.parse(jsonData).length;

        if (count > val) {
            array[0] = JSON.parse(jsonData)[val];
        }

        if (count > val + 1) {
            array[1] = JSON.parse(jsonData)[val + 1];
        } 

        if (count > val + 2) {
            array[2] = JSON.parse(jsonData)[val + 2];
        } 

        if (count > val + 3) {
            array[3] = JSON.parse(jsonData)[val + 3];
        }

        res.send(array);
});

const port = 5000;

app.listen(port, () => `Server running on port ${port}`);