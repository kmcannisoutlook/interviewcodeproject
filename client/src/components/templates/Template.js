import React, { useState, useEffect } from "react";

//Creates the filmstrip and populates with up to four thumbnails
//Clicking on thumbnail changes the displayed template
function Filmstrip({changeId}) {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(0);
  const [count, setCount] = useState(0);
  const [selected, setSelected] = useState('');
  const [btnPrev, setbtnPrev] = useState('');
  const [btnNext, setbtnNext] = useState('');


  useEffect(() => {

    fetch(`/api/filmstrip/${page}`)
      .then(response => response.json())
      .then(data => {
        setData(data)
        if(selected === '') {
          changeId(data[0].id)
          setSelected(data[0].id);
        }
      })
      
    fetch(`/api/template/count`)
      .then(response => response.json())
      .then(results => {
        setCount(results.count) 
        if (page !== 0) {
          setbtnPrev('')
        } else {
          setbtnPrev('disabled')
        }
        if (page + 4 <= results.count - 1) {
          setbtnNext('')
        } else {
          setbtnNext('disabled')
        }
      })

    }, [page], [count]);

    return (
      <div className="thumbnails">
			  <div className="group">

          {data.map(thumbnail => 
          <a href="#" className={selected === thumbnail.id ? 'active' : '' } title={thumbnail.id} key={thumbnail.id} onClick={() => {
              changeId(thumbnail.id); 
              setSelected(thumbnail.id);
            }}>
            <img src={'images/thumbnails/'+thumbnail.thumbnail} alt={thumbnail.id} width="145" height="121" />
            <span >{thumbnail.id}</span>
          </a>
          )}

          <span className={'previous '+btnPrev} title="Previous" onClick={() => {
            if (page !== 0) {
              setPage(page - 4)
            }
          }}>Previous</span>

          <span className={'next '+btnNext} title="Next" onClick={() => {
            if (page + 4 <= count - 1) {
              setPage(page + 4)
            }
          }}>Next</span>
          
        </div>
      </div>
    );
  }

//Creates the overall template display which including large image and text display, filmstrip and buttons
function Template() {
  const [data, setData] = useState({});
  const [id, setId] = useState('');

  useEffect(() => {
    if (id !== '') {
      fetch(`/api/template/${id}`)
      .then(response => response.json())
      .then(data => setData(data))
    }
    }, [id]);

  const changeId = id => {
    setId(id);
  }

  return (
    <div>
      <div id="large">
        <div className="group" key={data.id}>
          <img src={'images/large/'+data.image} alt="Large Image" width="430" height="360" />
          <div className="details">
            <p><strong>Title</strong> {data.title}</p>
            <p><strong>Description</strong> {data.descripton}</p>
            <p><strong>Cost</strong> ${data.cost}</p>
            <p><strong>ID #</strong> {data.id}</p>
            <p><strong>Thumbnail File</strong> {data.thumbnail}</p>
            <p><strong>Large Image File</strong> {data.image}</p>
          </div>
        </div>
      </div>
      <Filmstrip changeId={changeId}/>
    </div>
  );
}

export default Template;