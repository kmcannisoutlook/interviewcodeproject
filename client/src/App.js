import React, { Component } from 'react';
import Template from './components/templates/Template';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Template />
      </div>
    );
  }
}

export default App;